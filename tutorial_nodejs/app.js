const express = require('express'); // Include ExpressJS
var session = require('express-session');
var bodyParser = require('body-parser');
var path = require('path');
var con = require('./connectionDb');
var app = express();
app.use(session({
	secret: 'secret',
	resave: true,
	saveUninitialized: true
}));
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());
app.get('/', function(request, response) {
	response.sendFile(path.join(__dirname + '/views/login.html'));
});
app.post('/auth', function(request, response) {
	var username = request.body.username;
	var password = request.body.password;
	if (username && password) {

		con.query(`SELECT * FROM users WHERE username = '${username}' AND password = '${password}'`, function(error, results) {
            console.log(error);
			if (results.rowCount > 0) {
				request.session.loggedin = true;
				request.session.username = username;
				response.redirect('/home');
			} else {
				response.send('Incorrect Username and/or Password!');
			}			
			response.end();
		});
	} else {
		response.send('Please enter Username and Password!');
		response.end();
	}
});
app.get('/home', function(request, response) {
	if (request.session.loggedin) {
		response.send('Welcome back, ' + request.session.username + '!');
	} else {
		response.send('Please login to view this page!');
	}
	response.end();
});
app.listen(5000,()=>{
    console.log('Listening port 5000 ... ')
})
